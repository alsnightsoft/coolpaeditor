package com.alsnightsoft.editors.controllers

class AppController {

    def index() {
        redirect(url: '/app/')
    }
}
