package com.alsnightsoft.editors

import com.vaadin.server.VaadinRequest
import com.vaadin.ui.UI
import com.vaadin.ui.VerticalLayout

/**
 * Created by aluis on 10/18/14.
 */
class CoolPAEditor extends UI {

    private VerticalLayout mainLayout

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        mainLayout = new VerticalLayout()
        mainLayout.setSpacing(true)
        mainLayout.setMargin(true)

        setContent(mainLayout)
    }
}
